# Ed's Phaser Typescript Template and Asset Importer
Phaser is great, TypeScript is great and this template should help you get started with both.

## Recommended tools
* [Visual Studio Code](https://code.visualstudio.com) editor
* MIX Live Server plugin for VS Code.


# Usage
If you are not intending on using source control, download the repository from [here](https://bitbucket.org/edbishop/phaser-typescript/downloads) otherwise clone the repository as you would any other.

## Get the source and install dependencies.
```sh
$ git clone https://bitbucket.org/edbishop/phaser-typescript
$ npm i
```

If you have installed it, you can start the MIX Live Server plugin now if you want to start messing around immediately. It will load the game server with a live reload script embedded in the page and refresh it any time you create or update anything. If you rather you can just run the command below and open it in the browser then refresh it each tim eyou make a change.

## Serve the application locally on the default port
```sh
$ npm start
```

## Navigate to the application in your browser
> [http://localhost:8181](http://localhost:8181)

# Automatically importing and typing assets

## SAMPLE ASSETS
THE SAMPLE ASSETS USED ARE FROM THE CC LICENCED SUPERPOWERS ASSET PACK "Space Shooter" AND CAN BE FOUND HERE: [https://github.com/sparklinlabs/superpowers-asset-packs](https://github.com/sparklinlabs/superpowers-asset-packs)*

You can import assets automatically including generating the constants for their names using the AssetImporter mini application.
* .png
* .ogg
* .wav 

These should be placed in a sub folder of the Library for categorisation.
## Standard assets
```
src/Assets/Library/Backgrounds/blackHole.png
src/Assets/Library/Hud/energyBar.png
src/Assets/Library/Items/energyBar.png
src/Assets/Library/Music/track1.ogg
src/Assets/Library/Sounds/alert.wav
```
## PNG spritesheet assets
These should be named with the sprite width then height in the filename as follows
```
src/Assets/Library/Items/explosion_82x72.png
src/Assets/Library/Items/locator6_12x28.png
src/Assets/Library/Items/locator3_30x30.png
```

## Using the assets
This will create a file with the names as constants in it and a json file with the asset hierarchy in it. 

The json file is used to automatically import all of the assets when yourGame.loadAll() is called.