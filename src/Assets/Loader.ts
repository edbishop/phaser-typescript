import { assetMap } from './AssetMap';

export class Loader {
    public static loadAll(game: Phaser.Game) {
        Object.keys(assetMap.images).forEach(container => {
            assetMap.images[container].forEach(image => {
                game.load.image(image.name, image.file);
            });
        });
        Object.keys(assetMap.sounds).forEach(container => {
            assetMap.sounds[container].forEach(sound => {
                game.load.audio(sound.name, sound.file);
            });
        });
        Object.keys(assetMap.spriteSheets).forEach(container => {
            assetMap.spriteSheets[container].forEach(spritesheet => {
                game.load.spritesheet(spritesheet.name, spritesheet.file, spritesheet.width, spritesheet.height);
            });
        });
    }
}