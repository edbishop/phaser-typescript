import { writeFileSync } from 'fs';
import { basename, extname } from 'path';
import { sync } from 'glob';

class Importer {
    private static SPRITESHEET_REGEX = /_(\d*?)x(\d*?)\..*$/;
    private static NAME_REGEX = /^.*\/(.*)\.\w*?$/;
    private static CONTAINER_REGEX = /\/Library\/(\w*?)\//;
    private _assetMap: IAssetMap = { images: {}, sounds: {}, spriteSheets: {} };

    public import() {
        sync(__dirname + `/Library/**/*.*`).forEach(fullpath => {
            let file = '/src/Assets/' + fullpath.substring((__dirname + '/').length);
            let extension = extname(file).slice(1);

            if (!extension.length || !(<any>this)[extension]) return console.log('Unknown file type: ' + file);

            let name = Importer.NAME_REGEX.exec(file)[1];
            let container = Importer.CONTAINER_REGEX.exec(file)[1].toLowerCase();

            (<any>this)[extension](file, name, container);
        });

        writeFileSync(__dirname + '/AssetMap.ts', '/* tslint:disable */\n export let assetMap: IAssetMap = ' + JSON.stringify(this._assetMap, null, 4) + '\n/* tslint:enable */');

        let assetConstants = 'export class Library {\n';
        assetConstants += this.getConstantSection('images', this._assetMap.images);
        assetConstants += this.getConstantSection('audio', this._assetMap.sounds);
        assetConstants += this.getConstantSection('spriteSheets', this._assetMap.spriteSheets);
        assetConstants += '}';

        writeFileSync(__dirname + '/Library.ts', assetConstants);
    }

    private getConstantSection(name: string, map: { [container: string]: Array<IAsset> }) {
        let assetConstants = '    static ' + name + ' = {\n';
        Object.keys(map).forEach(container => {
            assetConstants += '        ' + container + ': {\n';
            map[container].forEach(item => {
                assetConstants += '            ' + item.name + ': \'' + item.name + '\',\n';
            });
            assetConstants += '        },\n';
        });
        assetConstants += '    };\n';
        return assetConstants;
    }

    private png(file: string, name: string, container: string) {
        let spriteDetails = Importer.SPRITESHEET_REGEX.exec(file);
        if (spriteDetails && spriteDetails.length === 3) {
            if (!this._assetMap.spriteSheets[container]) this._assetMap.spriteSheets[container] = [];
            let width = Number(spriteDetails[1]);
            let height = Number(spriteDetails[2]);
            this._assetMap.spriteSheets[container].push({ file, name, width, height });
        } else {
            if (!this._assetMap.images[container]) this._assetMap.images[container] = [];
            this._assetMap.images[container].push({ file, name });
        }
    }

    private wav = this.sound;
    private ogg = this.sound;
    private sound(file: string, name: string, container: string) {
        if (!this._assetMap.sounds[container]) this._assetMap.sounds[container] = [];
        this._assetMap.sounds[container].push({ file, name });
    }
}

let assetImporter = new Importer();
assetImporter.import();