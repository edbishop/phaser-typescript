declare interface IAsset {
    name: string;
    file: string;
}

declare interface IImage extends IAsset { }

declare interface ISound extends IAsset { }

declare interface ISpriteSheet extends IAsset {
    width: number;
    height: number;
}

declare interface IAssetMap {
    images: { [container: string]: Array<IImage> };
    spriteSheets: { [container: string]: Array<ISpriteSheet> };
    sounds: { [container: string]: Array<ISound> };
}