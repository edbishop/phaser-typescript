/* tslint:disable */
 export let assetMap: IAssetMap = {
    "images": {
        "backgrounds": [
            {
                "file": "/src/Assets/Library/Backgrounds/blackHole.png",
                "name": "blackHole"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/meteor1.png",
                "name": "meteor1"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/meteor2.png",
                "name": "meteor2"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/meteor3.png",
                "name": "meteor3"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/meteor4.png",
                "name": "meteor4"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/meteor5.png",
                "name": "meteor5"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/moon.png",
                "name": "moon"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/planet1.png",
                "name": "planet1"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/planet2.png",
                "name": "planet2"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/space1.png",
                "name": "space1"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/space2.png",
                "name": "space2"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/space3.png",
                "name": "space3"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/space4.png",
                "name": "space4"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/space5.png",
                "name": "space5"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/star1.png",
                "name": "star1"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/star2.png",
                "name": "star2"
            },
            {
                "file": "/src/Assets/Library/Backgrounds/stars.png",
                "name": "stars"
            }
        ],
        "hud": [
            {
                "file": "/src/Assets/Library/Hud/energyBar.png",
                "name": "energyBar"
            },
            {
                "file": "/src/Assets/Library/Hud/energyBarFill.png",
                "name": "energyBarFill"
            },
            {
                "file": "/src/Assets/Library/Hud/healthBar.png",
                "name": "healthBar"
            },
            {
                "file": "/src/Assets/Library/Hud/healthBarFill.png",
                "name": "healthBarFill"
            },
            {
                "file": "/src/Assets/Library/Hud/lifeEmpty.png",
                "name": "lifeEmpty"
            },
            {
                "file": "/src/Assets/Library/Hud/lifeFull.png",
                "name": "lifeFull"
            },
            {
                "file": "/src/Assets/Library/Hud/livesIcon.png",
                "name": "livesIcon"
            }
        ],
        "items": [
            {
                "file": "/src/Assets/Library/Items/ball.png",
                "name": "ball"
            },
            {
                "file": "/src/Assets/Library/Items/bomb.png",
                "name": "bomb"
            },
            {
                "file": "/src/Assets/Library/Items/bubble.png",
                "name": "bubble"
            },
            {
                "file": "/src/Assets/Library/Items/crate1.png",
                "name": "crate1"
            },
            {
                "file": "/src/Assets/Library/Items/crate2.png",
                "name": "crate2"
            },
            {
                "file": "/src/Assets/Library/Items/powerUp1.png",
                "name": "powerUp1"
            },
            {
                "file": "/src/Assets/Library/Items/powerUp2.png",
                "name": "powerUp2"
            },
            {
                "file": "/src/Assets/Library/Items/powerUp3.png",
                "name": "powerUp3"
            },
            {
                "file": "/src/Assets/Library/Items/powerUp4.png",
                "name": "powerUp4"
            },
            {
                "file": "/src/Assets/Library/Items/powerUp5.png",
                "name": "powerUp5"
            },
            {
                "file": "/src/Assets/Library/Items/rocket1.png",
                "name": "rocket1"
            },
            {
                "file": "/src/Assets/Library/Items/rocket2.png",
                "name": "rocket2"
            },
            {
                "file": "/src/Assets/Library/Items/shieldGreen.png",
                "name": "shieldGreen"
            },
            {
                "file": "/src/Assets/Library/Items/shieldPink.png",
                "name": "shieldPink"
            },
            {
                "file": "/src/Assets/Library/Items/ship1.png",
                "name": "ship1"
            },
            {
                "file": "/src/Assets/Library/Items/ship10.png",
                "name": "ship10"
            },
            {
                "file": "/src/Assets/Library/Items/ship2.png",
                "name": "ship2"
            },
            {
                "file": "/src/Assets/Library/Items/ship3.png",
                "name": "ship3"
            },
            {
                "file": "/src/Assets/Library/Items/ship4.png",
                "name": "ship4"
            },
            {
                "file": "/src/Assets/Library/Items/ship5.png",
                "name": "ship5"
            },
            {
                "file": "/src/Assets/Library/Items/ship6.png",
                "name": "ship6"
            },
            {
                "file": "/src/Assets/Library/Items/ship7.png",
                "name": "ship7"
            },
            {
                "file": "/src/Assets/Library/Items/ship8.png",
                "name": "ship8"
            },
            {
                "file": "/src/Assets/Library/Items/ship9.png",
                "name": "ship9"
            },
            {
                "file": "/src/Assets/Library/Items/shot1.png",
                "name": "shot1"
            },
            {
                "file": "/src/Assets/Library/Items/shot10.png",
                "name": "shot10"
            },
            {
                "file": "/src/Assets/Library/Items/shot11.png",
                "name": "shot11"
            },
            {
                "file": "/src/Assets/Library/Items/shot12.png",
                "name": "shot12"
            },
            {
                "file": "/src/Assets/Library/Items/shot2.png",
                "name": "shot2"
            },
            {
                "file": "/src/Assets/Library/Items/shot3.png",
                "name": "shot3"
            },
            {
                "file": "/src/Assets/Library/Items/shot4.png",
                "name": "shot4"
            },
            {
                "file": "/src/Assets/Library/Items/shot5.png",
                "name": "shot5"
            },
            {
                "file": "/src/Assets/Library/Items/shot6.png",
                "name": "shot6"
            },
            {
                "file": "/src/Assets/Library/Items/shot7.png",
                "name": "shot7"
            },
            {
                "file": "/src/Assets/Library/Items/shot8.png",
                "name": "shot8"
            },
            {
                "file": "/src/Assets/Library/Items/shot9.png",
                "name": "shot9"
            }
        ],
        "splash": [
            {
                "file": "/src/Assets/Library/Splash/phaser.png",
                "name": "phaser"
            }
        ]
    },
    "sounds": {
        "music": [
            {
                "file": "/src/Assets/Library/Music/track1.ogg",
                "name": "track1"
            },
            {
                "file": "/src/Assets/Library/Music/track2.ogg",
                "name": "track2"
            },
            {
                "file": "/src/Assets/Library/Music/track3.ogg",
                "name": "track3"
            },
            {
                "file": "/src/Assets/Library/Music/track4.ogg",
                "name": "track4"
            }
        ],
        "sounds": [
            {
                "file": "/src/Assets/Library/Sounds/alert.wav",
                "name": "alert"
            },
            {
                "file": "/src/Assets/Library/Sounds/bossDeath.wav",
                "name": "bossDeath"
            },
            {
                "file": "/src/Assets/Library/Sounds/darkShoot.wav",
                "name": "darkShoot"
            },
            {
                "file": "/src/Assets/Library/Sounds/death.wav",
                "name": "death"
            },
            {
                "file": "/src/Assets/Library/Sounds/success1.wav",
                "name": "success1"
            },
            {
                "file": "/src/Assets/Library/Sounds/success2.wav",
                "name": "success2"
            },
            {
                "file": "/src/Assets/Library/Sounds/success3.wav",
                "name": "success3"
            },
            {
                "file": "/src/Assets/Library/Sounds/success4.wav",
                "name": "success4"
            }
        ]
    },
    "spriteSheets": {
        "fonts": [
            {
                "file": "/src/Assets/Library/Fonts/alphaNumericFont_42x33.png",
                "name": "alphaNumericFont_42x33",
                "width": 42,
                "height": 33
            },
            {
                "file": "/src/Assets/Library/Fonts/fullFont_20x20.png",
                "name": "fullFont_20x20",
                "width": 20,
                "height": 20
            }
        ],
        "items": [
            {
                "file": "/src/Assets/Library/Items/explosion_82x72.png",
                "name": "explosion_82x72",
                "width": 82,
                "height": 72
            },
            {
                "file": "/src/Assets/Library/Items/locator1_38x38.png",
                "name": "locator1_38x38",
                "width": 38,
                "height": 38
            },
            {
                "file": "/src/Assets/Library/Items/locator2_34x34.png",
                "name": "locator2_34x34",
                "width": 34,
                "height": 34
            },
            {
                "file": "/src/Assets/Library/Items/locator3_30x30.png",
                "name": "locator3_30x30",
                "width": 30,
                "height": 30
            },
            {
                "file": "/src/Assets/Library/Items/locator4_36x36.png",
                "name": "locator4_36x36",
                "width": 36,
                "height": 36
            },
            {
                "file": "/src/Assets/Library/Items/locator5_14x28.png",
                "name": "locator5_14x28",
                "width": 14,
                "height": 28
            },
            {
                "file": "/src/Assets/Library/Items/locator6_12x28.png",
                "name": "locator6_12x28",
                "width": 12,
                "height": 28
            },
            {
                "file": "/src/Assets/Library/Items/locator6_12x29.png",
                "name": "locator6_12x29",
                "width": 12,
                "height": 29
            }
        ]
    }
}
/* tslint:enable */