import { readFileSync, writeFileSync } from 'fs';
import { sync } from 'glob';

// Update phaser typings reference
let oldTypingsRef = 'typescript/typings.json';
let newTypingsRef = 'typescript/phaser.comments.d.ts';
sync('**/phaser/package.json').forEach(phaserPackageJson => {
    let packageJson = readFileSync(phaserPackageJson, 'utf8');
    packageJson = packageJson.replace(oldTypingsRef, newTypingsRef);
    writeFileSync(phaserPackageJson, packageJson, 'utf8');
});
console.log('Updated the typings reference in Phaser.');