import * as Phaser from 'phaser';
import { Loader } from './Assets/Loader';
import { IGameState } from './IGameState';

export class Game {
    public instance: Phaser.Game;

    constructor(width?: number | string, height?: number | string, renderer?: number, parent?: any, state?: IGameState, transparent?: boolean, antialias?: boolean, physicsConfig?: any) {
        this.instance = new Phaser.Game(width, height, renderer, parent, state, transparent, antialias, physicsConfig);
    }

    public loadAll() {
        Loader.loadAll(this.instance);
    }
}