import * as Phaser from 'phaser';
import { Loader } from './Assets/Loader';
import { Library } from './Assets/Library';

let game: Phaser.Game = new Phaser.Game('100%', '100%', Phaser.AUTO, 'myGame', {
    /**
     * Here you can pass all of the various state methods. 
     * I'm passing a preload at this point that loads all 
     * assets  using a helper method on my Game class...
     * (a bad idea in a "real" game :o)
     */
    preload: () => Loader.loadAll(game),
    create: () => {
        let splash = game.add.sprite(game.world.centerX, game.world.centerY, Library.images.splash.phaser);
        splash.anchor.setTo(0.5, 0.5);
    }
});